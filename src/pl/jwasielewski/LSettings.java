package pl.jwasielewski;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/**
 * Klasa obsługująca wczytywanie/zapisywanie ustawień programu
 * @author Jędrzej Wasielewski
 */
public class LSettings {

    private static final String LINUX_SETTINGS_PATH =
            "/home/" + System.getenv("USER") + "/.lara/settings";
    private static final String WINDOWS_SETTINGS_PATH =
            System.getenv("appdata") + "\\Lara\\settings";
    private String os;
    private FileReader fr;
    private BufferedReader bfr;
    private FileWriter fw;
    private BufferedWriter bfw;

    public LSettings() {
        os = System.getProperty("os.name").toLowerCase();
    }

    /**
     * Metoda zwraca ustawienia programu zapisane w pliku konfiguracyjnym
     * @return Vector z ustawieniami zapisanymi jako String w formacie "nazwa|rok,miesiąc,dzień,godzina,minuta|x,y"
     * @throws Exception
     */
    public Vector<String> readSettings() throws Exception {
        Vector<String> vs = new Vector<String>();
        String line;

        try {
            if(os.contains("linux")) {
                createParentDirectory(LINUX_SETTINGS_PATH);
                fr = new FileReader(LINUX_SETTINGS_PATH);
            }
            else if(os.contains("windows")) {
                createParentDirectory(WINDOWS_SETTINGS_PATH);
                fr = new FileReader(WINDOWS_SETTINGS_PATH);
            }
            else {
                throw new Exception("Unsupported OS!");
            }
        }
        catch (IOException ioe) {
            System.out.println("Error while opening settings file!");
            ioe.printStackTrace();
            System.exit(2);
        }

        try {
            bfr = new BufferedReader(fr);
            while((line = bfr.readLine()) != null) {
                vs.add(line);
            }
        }
        catch (IOException ioe) {
            System.out.println("Error while reading settings file!");
            ioe.printStackTrace();
            System.exit(3);
        }

        try {
            fr.close();
        }
        catch (IOException ioe) {
            System.out.println("Error while closing settings file!");
            ioe.printStackTrace();
            System.exit(4);
        }

        return vs;
    }

    /**
     * Metoda zapisuje ustawienia programu do pliku konfiguracyjnego
     * @param vs Vector z ustawieniami zapisanymi jako String w formacie "nazwa|rok,miesiąc,dzień,godzina,minuta|x,y"
     * @throws Exception
     */
    public void writeSettings(Vector<String> vs) throws Exception {
        //System.out.println("[DEBUG] Entered to writeSettings method");
        try {
            if(os.contains("linux")) {
                fw = new FileWriter(LINUX_SETTINGS_PATH);
            }
            else if(os.contains("windows")) {
                //System.out.println("[DEBUG] os.contains => " + os.contains("windows"));
                fw = new FileWriter(WINDOWS_SETTINGS_PATH);
            }
            else {
                throw new Exception("Unsupported OS!");
            }
        }
        catch (IOException ioe) {
            System.out.println("Error while opening settings file!");
            ioe.printStackTrace();
            System.exit(2);
        }

        try {
            bfw = new BufferedWriter(fw);
            for(String line: vs) {
                bfw.write(line);
                bfw.newLine();
                //System.out.println("[DEBUG] BufferedWriter: " + line);
            }
        }
        catch (IOException ioe) {
            System.out.println("Error while writing settings file!");
            ioe.printStackTrace();
            System.exit(3);
        }

        try {
            bfw.flush();
            //System.out.println("[DEBUG] fw.flush");
            bfw.close();
            //System.out.println("[DEBUG] fw.close");
        }
        catch (IOException ioe) {
            System.out.println("Error while closing settings file!");
            ioe.printStackTrace();
            System.exit(4);
        }
    }

    /**
     * Utworzenie katalogu przechowywującego ustawienia programu
     * @param ścieżka do tworzonego katalogu
     * @throws IOException
     */
    private void createParentDirectory(String path) throws IOException {
        File f = new File(path);
        File dir = f.getParentFile();
        if(!dir.exists()) {
            dir.mkdir();
        }
        if(!f.exists()) {
            f.createNewFile();
        }
    }
}
